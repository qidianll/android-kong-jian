package com.example.f;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


public class FileParse {

    public static void main(String[] args) throws IOException {

        String inPath = "/home/qidian/桌面/kkc.txt";
        File inFile = new File(inPath);
        if (inFile.exists()) {
//            long startTime = System.currentTimeMillis();
//            for (int aaa= 1;aaa<1000;aaa++){
            String conent = parseFile2(inFile);
//            }
//            long endTime = System.currentTimeMillis();
//            System.out.println("总共发费时间 : " +(endTime-startTime) +"毫秒");
            String outPath = "/home/qidian/桌面/qwer11.txt";
            writeToFile(conent, outPath);
        } else {
            //TODO print
        }
    }

    public static boolean writeToFile(String content, String path) {
        File file = new File(path);
        try {
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String parseFile2(File file) throws IOException {

        Map<String, StringBuffer> strMap = new LinkedHashMap<>();
        BufferedReader bufferedReader = null;
        FileInputStream fileReader = new FileInputStream(file);
        bufferedReader = new BufferedReader(new InputStreamReader(fileReader));

        if (bufferedReader != null) {
            String str = null;
            StringBuffer strBuffer = null;
            int index = 0;
            bufferedReader.readLine();
            while ((str = bufferedReader.readLine()) != null && str.length() > 0) {
                String[] strSplit = str.split(",");
                if (strSplit.length == 6) {
                    if (strSplit[1].length() > 0) {
                        strBuffer = strMap.get(strSplit[1]);
                        if (strBuffer == null) {
                            strBuffer = new StringBuffer();
                            strBuffer.append(strSplit[3] + ",");
                            strMap.put(strSplit[1], strBuffer);
                            index = 1;
                        } else {

                            if (index == 0) {
                                strBuffer.append("\n              ");
                            }
                            index++;
                            strBuffer.append(strSplit[3] + ",");
                            if (index == 3) {
                                index = 0;
                            }
                        }
                    }
                }
            }
        }


        StringBuffer stringBuffer = new StringBuffer();
        Iterator<Map.Entry<String, StringBuffer>> iterator = strMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, StringBuffer> entry = iterator.next();
            if (stringBuffer.length() == 0) {
                stringBuffer.append("id = " + entry.getKey() + ",data = " + entry.getValue());
            } else {
                stringBuffer.append("\nid = " + entry.getKey() + ",data = " + entry.getValue());
            }
        }
        return stringBuffer.toString();
    }


    public static String parseFile(File file) throws IOException {
        StringBuffer sb = new StringBuffer();
        BufferedReader bufferedReader = null;
        FileInputStream fileReader = new FileInputStream(file);
        bufferedReader = new BufferedReader(new InputStreamReader(fileReader));

        if (bufferedReader != null) {
            String str = null;
            int index = 0;
            bufferedReader.readLine();
            while ((str = bufferedReader.readLine()) != null && str.length() > 0) {
                String[] strSplit = str.split(",");
                if (strSplit.length == 6) {
                    if (strSplit[1].length() > 0) {
                        if (sb.toString().contains(strSplit[1])) {
                            if (index == 0) {
                                sb.append("\n              ");
                            }
                            sb.append(strSplit[3] + ",");
                            index++;
                            if (index == 3) {
                                index = 0;
                            }

                        } else {
                            if (index == 0) {
                                sb.append("id = " + strSplit[1] + ",data = " + strSplit[3] + ",");
                            } else {
                                sb.append("\nid = " + strSplit[1] + ",data = " + strSplit[3] + ",");
                            }
                            index = 1;
                        }
                    }
                }

            }
        }
        return sb.toString();
    }
}
