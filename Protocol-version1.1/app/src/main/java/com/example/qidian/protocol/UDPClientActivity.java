package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPClientActivity extends AppCompatActivity implements View.OnClickListener {

//    final static String TAG = "UDPClientActivity: ";
    EditText etHost, etPort, etSend;
    Button btnStart, btnSend;
    TextView tvSession;
    DatagramSocket socket;

    String ip;
    int port;
    boolean isOpened;

    static Handler sHandler = new Handler();

    public Runnable mReiverRunnable = new Runnable() {
        public void run() {
//            Log.e(TAG,"ReceiverRunnable start");
            byte[] buf = new byte[SocketUtil.REV_BUFFER_LEN];
            DatagramPacket dp = new DatagramPacket(buf,buf.length);
            while (isOpened){
                try {
                    socket.receive(dp);
                    final String text = new String(dp.getData(),0,dp.getLength());
                    sHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvSession.append("ip地址："+ip+"端口号："+port+"消息："+text+"\n");
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    closeSocket();
                    break;
                }
            }
//            Log.e(TAG,"ReceiverRunnable end");
        }
    };

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        setupViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isOpened){
            closeSocket();
        }
    }

    private void setupViews(){
        etHost = findViewById(R.id.etHost);
        etPort = findViewById(R.id.edit1);
        btnStart = findViewById(R.id.btnOpenClose);
        tvSession = findViewById(R.id.text3);
        btnSend = findViewById(R.id.bottom1);
        etSend = findViewById(R.id.edit2);
        btnStart.setText("开始");
        btnStart.setOnClickListener(this);
        btnSend.setOnClickListener(this);
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.btnOpenClose:
                if (isOpened){
                    closeSocket();
                }else{
                    openSocket();
                }
                break;
            case R.id.bottom1:
                if (isOpened){
                    String str = etSend.getText().toString();
                    if (!TextUtils.isEmpty(str)){
                        send(str);
                    }else{
                        showToast("请输入发送内容");
                    }
                }else{
                    showToast("请先点击开始再发送");
                }
                break;
            default:
                    break;
        }
    }

    private void openSocket(){
        if (checkValidAddress()){
            try {
                socket = new DatagramSocket();
                isOpened = true;
            } catch (SocketException e) {
                e.printStackTrace();
                socket = null;
                isOpened = false;
            }

            if (isOpened){
                new Thread(mReiverRunnable).start();
            }
            btnStart.setText("关闭");
        }
    }

    private void closeSocket(){
        if (socket != null){
            socket.close();
            socket = null;
        }
        btnStart.setText("开始");
        isOpened = false;
    }

    private void send(final String text){
        new Thread(mSendRunnable).start();
        tvSession.append("发送:"+ text + "\n");
}

    private boolean checkValidAddress(){
        String ip = etHost.getText().toString();
        if(!SocketUtil.isValidHost(ip)){
            showToast("请输入有效的IP");
            return false;
        }

        String port = etPort.getText().toString();
        if (!SocketUtil.isValidPort(port)){
            showToast("请输入有效的Port");
            return false;
        }
        this.ip = ip;
        this.port = Integer.parseInt(port);
        return true;
    }

    private void showToast(String text){
        Toast.makeText(UDPClientActivity.this,text,Toast.LENGTH_SHORT).show();
    }

    private Runnable mSendRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                byte[] b =  etSend.getText().toString().getBytes();
                final DatagramPacket dp = new DatagramPacket(b, b.length, InetAddress.getByName(ip), port);
                socket.send(dp);
            } catch (IOException e) {
                e.printStackTrace();
                closeSocket();
            }
        }
    };
}
