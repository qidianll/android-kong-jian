package com.example.qidian.protocol;

import android.text.TextUtils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SocketUtil {

    public static final int REV_BUFFER_LEN = 1024*2;

    public  static  boolean isValidHost(String host){

        if (!TextUtils.isEmpty(host)){
            String rexp = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."+
                    "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."+
                    "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."+
                    "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
            Pattern pat = Pattern.compile(rexp);
            Matcher mat = pat.matcher(host);
            boolean ipAddress = mat.find();
            return ipAddress;
        }
        return false;
    }

    public static  boolean isValidPort(String port){
        try{
           int iPort = Integer.parseInt(port);
           if (iPort > 0 && iPort< Short.MAX_VALUE-Short.MIN_VALUE){
               return true;
           }
           return false;
        }catch (NumberFormatException e){
            e.printStackTrace();
            return false;
        }
    }
}
