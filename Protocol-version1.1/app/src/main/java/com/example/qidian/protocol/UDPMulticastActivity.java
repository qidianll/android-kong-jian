package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;

public class UDPMulticastActivity extends AppCompatActivity implements View.OnClickListener {

//    final static String TAG = "UDPClientActivity: ";

    EditText etAddress, etPort, etContent;
    Button btnStart, btnSure;
    TextView tvSession;
    MulticastSocket socket;
    InetAddress address;

    int port;
    String str, ip;
    boolean isOpened;

    static Handler handler = new Handler();

    public Runnable mReceiveRunnable = new Runnable() {
        public void run() {
            try {
                byte[] b = new byte[1024];
                DatagramPacket dp = new DatagramPacket(b, b.length);
                while (isOpened) {
                    socket.receive(dp);
                    final String data = new String(dp.getData(), 0, dp.getLength());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvSession.append("接收消息为:" + data + "\n");
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                closeSocket();
            }
        }
    };

    public Runnable mSendRunnable = new Runnable() {
        public void run() {
            try {
                byte[] b = etContent.getText().toString().getBytes();
                DatagramPacket dp = new DatagramPacket(b, b.length, address, port);
                socket.send(dp);
            } catch (Exception e) {
                e.printStackTrace();
                closeSocket();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_udpbroadcast);

        setupViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isOpened) {
            closeSocket();
        }
    }

    private void setupViews() {
        etAddress = findViewById(R.id.etHost);
        etPort = findViewById(R.id.edit1);
        btnStart = findViewById(R.id.btnOpenClose);
        tvSession = findViewById(R.id.text3);
        btnSure = findViewById(R.id.bottom1);
        etContent = findViewById(R.id.edit2);
        btnStart.setText("开始");
        btnStart.setOnClickListener(this);
        btnSure.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOpenClose:
                if (isOpened) {
                    closeSocket();
                } else {
                    openSocket();
                }
                break;
            case R.id.bottom1:
                if (isOpened) {
                    str = etContent.getText().toString();
                    if (!TextUtils.isEmpty(str)) {
                        send(str);
                    } else {
                        showToast("请输入发送内容");
                    }
                } else {
                    showToast("请先点击开始再发送");
                }
                break;
            default:
                break;
        }
    }

    private void openSocket() {
        if (checkValidAddress()) {
            try {
//                Log.e(TAG, "ReceiverRunnable start11");
                socket = new MulticastSocket(port);
                address = InetAddress.getByName(ip);
                socket.joinGroup(address);
                isOpened = true;
            } catch (IOException e) {
                e.printStackTrace();
                socket = null;
                isOpened = false;
            }
            if (isOpened) {
                new Thread(mReceiveRunnable).start();
            }
            btnStart.setText("关闭");
        }
    }

    private void closeSocket() {
        if (socket != null) {
            socket.close();
            socket = null;
        }
        btnStart.setText("开始");
        isOpened = false;
    }

    private void send(final String text) {
        new Thread(mSendRunnable).start();
        tvSession.append("发送:" + text + "\n");
    }

    private boolean checkValidAddress() {
        String ip = etAddress.getText().toString();
        if (!SocketUtil.isValidHost(ip)) {
            showToast("请输入有效的IP");
            return false;
        }

        String port = etPort.getText().toString();
        if (!SocketUtil.isValidPort(port)) {
            showToast("请输入有效的Port");
            return false;
        }
        this.ip = ip;
        this.port = Integer.parseInt(port);
        return true;
    }

    private void showToast(String text) {
        Toast.makeText(UDPMulticastActivity.this, text, Toast.LENGTH_SHORT).show();
    }
}
