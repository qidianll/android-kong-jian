package com.example.qidian.protocol;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    public void TCPClientActivity(View view){
        startActivity(TCPClientActivity.class);
    }
    public void UDPClientActivity(View view){
        startActivity(UDPClientActivity.class);
    }
    public void TCPServerActivity(View view){
        startActivity( TCPServerActivity.class);
    }
    public void UDPServerActivity(View view){
        startActivity( UDPServerActivity.class);
    }
    public void UDPBroadcastServerActivity(View view){
        startActivity(UDPBroadcastServerActivity.class);
    }
    public void UDPMulticastActivity(View view){
        startActivity(UDPMulticastActivity.class);
    }
    public void UDPBroadcastClient(View view){
        startActivity(UDPBroadcastClient.class);
    }
    private void startActivity(Class<?> cls){
        startActivity(new Intent(this, cls));
    }
}
