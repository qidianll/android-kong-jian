package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPBroadcastClient extends AppCompatActivity implements View.OnClickListener {

    EditText etPort, etcontent;
    Button btnStart, btnSend;
    TextView tvSession;
    boolean isOpened;
    int portC, portS;
    String str, line;
    DatagramSocket socket;

    static Handler handler = new Handler();

    public Runnable mReiverRunnable = new Runnable() {
        public void run() {
            try {
                byte[] b = new byte[1024];
                DatagramPacket packet = new DatagramPacket(b, b.length);
                while (isOpened) {
                    socket.receive(packet);
                    final String ip = packet.getAddress().getHostAddress();
                    line = ip;
                    final String data = new String(packet.getData(), 0, packet.getLength());
                    portC = packet.getPort();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvSession.append("ip地址：" + ip + "端口号：" + portC + "消息：" + data + "\n");
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                closeSocket();
            }
        }
    };

    private Runnable mSendRunnable = new Runnable() {
        public void run() {
            try {
                if (isOpened && str.length() != 0) {
                    byte[] b = str.getBytes();
                    DatagramPacket dd = new DatagramPacket(b, b.length, InetAddress.getByName(line), portC);
                    socket.send(dd);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvSession.append("发送:" + str + "\n");
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
                closeSocket();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_udpcc);

        setupViews();
    }

    private void setupViews() {
        etPort = findViewById(R.id.etHost);
        btnStart = findViewById(R.id.btnOpenClose);
        tvSession = findViewById(R.id.text3);
        btnSend = findViewById(R.id.bottom1);
        etcontent = findViewById(R.id.edit2);
        btnStart.setText("开始");
        btnStart.setOnClickListener(this);
        btnSend.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOpenClose:
                if (isOpened) {
                    closeSocket();
                } else {
                    btnStart.setText("取消");
                    isOpened = true;
                }
                String pp = etPort.getText().toString();
                portS = Integer.parseInt(pp);
                try {
                    socket = new DatagramSocket(portS);
                } catch (SocketException e) {
                    e.printStackTrace();
                }
                new Thread(mReiverRunnable).start();
                break;
            case R.id.bottom1:
                if (isOpened) {
                    str = etcontent.getText().toString();
                    new Thread(mSendRunnable).start();
                } else {
                    Toast.makeText(UDPBroadcastClient.this, "请点击开始", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isOpened){
            closeSocket();
        }
    }

    private void closeSocket(){
        if (socket != null){
            socket.close();
            socket = null;
        }
        btnStart.setText("开始");
        isOpened = false;
    }
}