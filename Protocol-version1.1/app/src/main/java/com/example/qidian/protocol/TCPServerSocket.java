package com.example.qidian.protocol;

import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TCPServerSocket {

    final static String TAG = "TCPSocket: ";

    public interface TCPServerSocketCallback {
        void onRead(byte[] data, int length);

        void onSocketState(boolean isOpened);
    }
    static Handler sHandler = new Handler();


    private TCPServerSocket.TCPServerSocketCallback mCallback;
    private int mPort;
    private boolean isOpened;
    private ServerSocket mSocket;
    private InputStream mInputStream;
    private Socket skt;

    private LinkedBlockingQueue<byte[]> mWriteQueue = new LinkedBlockingQueue<>();
    public static List<Object> socketList= Collections.synchronizedList(new ArrayList<>());

    public TCPServerSocket( int port) {
        this.mPort = port;
    }

    public void setSocketCallback(TCPServerSocket.TCPServerSocketCallback callback) {
        this.mCallback = callback;
    }

    public void openSocket() {
        if (!isOpened) {
            isOpened = true;
            new Thread(mCreateAndWriteRunnable).start();
        }
    }

    public void closeSocket() {
        isOpened = false;
    }

    public void write(byte[] data) {
        mWriteQueue.add(data);
    }

    private void onReadcallback(final byte[] data, final int len) {
        if (mCallback != null) {
            sHandler.post(new Runnable() {
                @Override
                public void run() {
                    mCallback.onRead(data, len);
                }
            });
        }
    }

    private void onStatecallback(final boolean state) {
        if (mCallback != null) {
            sHandler.post(new Runnable() {
                @Override
                public void run() {
                    mCallback.onSocketState(state);
                }
            });
        }
    }

    private Runnable mReadRunnable = new Runnable() {
        @Override
        public void run() {
            byte[] buffer = new byte[1024];
//            Log.e(TAG, " start read thread");

            while (isOpened) {
                try {
                    int len = mInputStream.read(buffer);
                    if (len >= 0) {
                        onReadcallback(buffer, len);
                    } else if (len == -1) {
                        isOpened = false;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    isOpened = false;
                }

            }
            Log.e(TAG, " end read thread");
        }
    };

    private Runnable mCreateAndWriteRunnable = new Runnable() {
        @Override
        public void run() {
            OutputStream outputStream = null;
            try {
                mSocket = new ServerSocket( mPort);
                sHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mCallback.onSocketState(isOpened);
                    }
                });
                skt = mSocket.accept();
                socketList.add(skt);
                outputStream = skt.getOutputStream();
                mInputStream = skt.getInputStream();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                isOpened = false;
            } catch (IOException e) {
                e.printStackTrace();
                isOpened = false;
            }

            Log.e(TAG, " start write thread" + mPort + isOpened);
            if (isOpened) {
                onStatecallback(isOpened);
                new Thread(mReadRunnable).start();

            }
            while (isOpened) {
                try {
                    byte[] data = mWriteQueue.poll(100, TimeUnit.MILLISECONDS);
                    if (data != null) {
                        outputStream.write(data);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    isOpened = false;
                } catch (IOException e) {
                    e.printStackTrace();
                    isOpened = false;
                }
            }

            try {
                if (skt != null) {
                    skt.close();
                    skt = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            onStatecallback(isOpened);

            Log.e(TAG, " end write thread");
        }
    };
}
