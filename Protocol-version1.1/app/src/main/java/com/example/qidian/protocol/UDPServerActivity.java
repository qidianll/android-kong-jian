package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPServerActivity extends AppCompatActivity implements View.OnClickListener{
    final static String TAG = "UDPClientActivity: ";

    EditText etPort, etSend;
    Button btnStart, btnSend;
    TextView tvSession;
    DatagramSocket socket;

    boolean isOpened;
    int port ,portC;
    String str,line;
    byte[] by;

    static Handler handler = new Handler();

    public Runnable mReiverRunnable = new Runnable(){
        public void run() {
            Log.e(TAG,"ReceiverRunnable start");
                by = new byte[1024];
                DatagramPacket dp = new DatagramPacket(by,by.length);
                while(isOpened){
                    try {
                        socket.receive(dp);
                    } catch (IOException e) {
                        e.printStackTrace();
                        closeSocket();
                        break;
                    }
                    final String ip = dp.getAddress().getHostAddress();
                    line = ip;
                    final String data = new String(dp.getData(),0,dp.getLength());
                    portC=dp.getPort();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            tvSession.append("ip地址："+ip+"端口号："+portC+"接收消息为："+data+"\n");
                        }
                    });
                }
            Log.e(TAG,"ReceiverRunnable end");
            }
        };

    private Runnable mSendRunnable = new Runnable(){
        public void run() {
            try{
                if (isOpened &&str.length()!=0&&line != null) {
                        byte[] b = str.getBytes();
                        DatagramPacket dp = new DatagramPacket(b, b.length,InetAddress.getByName(line), portC);
                        socket.send(dp);
                }
            }catch (IOException e){
                e.printStackTrace();
                closeSocket();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showToast("请等待客户端连接");
                    }
                });
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        setupViews();
    }

    private void setupViews(){
        etPort = findViewById(R.id.etHost);
        btnStart = findViewById(R.id.btnOpenClose);
        tvSession = findViewById(R.id.text3);
        btnSend = findViewById(R.id.bottom1);
        etSend = findViewById(R.id.edit2);
        btnStart.setText("开始");
        btnStart.setOnClickListener(this);
        btnSend.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOpenClose:
                if (isOpened) {
                    closeSocket();
                } else {
                    openSocket();
                }    break;
            case R.id.bottom1:
                if (isOpened){
                    str = etSend.getText().toString();
                    if (!TextUtils.isEmpty(str)){
                        send(str);
                    }else{
                        showToast("请输入发送内容");
                    }
                }else{
                    showToast("请先点击开始再发送");
                }
                break;
            default:
                break;
        }
    }

    private void send(final String text){
        new Thread(mSendRunnable).start();
        tvSession.append("发送:"+ text + "\n");
    }

    private void openSocket(){
        if (checkValidAddress()){
            try {
                socket = new DatagramSocket(port);
                isOpened = true;
            } catch (SocketException e) {
                e.printStackTrace();
                socket = null;
                isOpened = false;
            }

            if (isOpened){
                new Thread(mReiverRunnable).start();
            }
            btnStart.setText("关闭");
        }
    }

    private void closeSocket(){
            if (socket != null){
                socket.close();
                socket = null;
            }
            btnStart.setText("开始");
            isOpened = false;
        }

    private boolean checkValidAddress(){

        String port = etPort.getText().toString();
        if (!SocketUtil.isValidPort(port)){
            showToast("请输入有效的Port");
            return false;
        }
        this.port = Integer.parseInt(port);
        return true;
    }

    private void showToast(String text){
        Toast.makeText(UDPServerActivity.this,text,Toast.LENGTH_SHORT).show();
    }
}
