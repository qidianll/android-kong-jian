package com.example.qidian.protocol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TCPServerActivity extends AppCompatActivity implements View.OnClickListener ,TCPServerSocket.TCPServerSocketCallback {

    EditText etPort, etContent;
    Button btnStart, btnSend;
    TextView tvSession;
    TCPServerSocket socket;
//    ServerSocket ss;

    boolean isOpened;
//    String str;
    int port;

//    public static List<Object> socketList = Collections.synchronizedList(new ArrayList<>());

//    public class mSendRunnable implements Runnable {
//        Socket s;
//        OutputStream outputStream;
//
//        public mSendRunnable(Socket s) throws IOException {
//            this.s = s;
//            outputStream = socket.getOutputStream();
//        }
//
//        public void run() {
//            try {
//                if (s.isConnected()) {
//                    byte[] out = str.getBytes();
//                    outputStream.write(out, 0, out.length);
//                    tvSession.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            tvSession.append("发送消息为：" + str + "\n");
//                        }
//                    });
//                } else {
//                    showToast("等待连接");
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            socketList.remove(this.s);
//        }
//    }
//
//    public class mReceiverRunnable implements Runnable {
//        Socket s;
//        InputStream inputStream;
//
//        public mReceiverRunnable(Socket s) throws IOException {
//            this.s = s;
//            inputStream = s.getInputStream();
//        }
//
//        public void run() {
//            try {
//                byte[] b = new byte[1024];
//                while (s.isConnected()) {
//                    int len = inputStream.read(b, 0, b.length);
//                    final String content = new String(b, 0, len);
//                    tvSession.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            tvSession.append("接收到消息：" + content + "\n");
//                        }
//                    });
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        setupViews();
    }

    private void setupViews() {
        etPort = findViewById(R.id.etHost);
        btnStart = findViewById(R.id.btnOpenClose);
        tvSession = findViewById(R.id.text3);
        btnSend = findViewById(R.id.bottom1);
        etContent = findViewById(R.id.edit2);
        btnStart.setText("开始");
        btnStart.setOnClickListener(this);
        btnSend.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (socket != null){
            socket.setSocketCallback(null);
            socket.closeSocket();
        }
    }

    @Override
    public void onRead(byte[] data, int length) {
        Log.e("read: ",""+length);

        String rev = null;
        try {
            rev = new String(data,0,length,"utf-8");
            tvSession.append(rev);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSocketState(boolean isOpened) {
        this.isOpened = isOpened;
        if (isOpened){
            btnStart.setText("关闭");
        }else{
            btnStart.setText("开始");
            socket = null;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOpenClose:
                if(isOpened){
                    isOpened = false;
                    socket.closeSocket();
                    socket = null;
                }else{
                    if (socket == null) {
                        if (checkValidAddress()) {
                            socket = new TCPServerSocket(port);
                            socket.setSocketCallback(this);
                            socket.openSocket();
                        }
                    }
                }
                break;
            case R.id.bottom1:
                if (isOpened) {
                    String str = etContent.getText().toString();
                    if (!TextUtils.isEmpty(str)) {
                        socket.write(str.getBytes());
                    } else {
                        showToast("请输入发送内容");
                    }
                } else {
                    showToast("请先点击开始再发送");
                }
                break;
            default:
                break;
        }
    }

    private boolean checkValidAddress(){
        String port = etPort.getText().toString();
        if (!SocketUtil.isValidPort(port)){
            showToast("请输入有效的Port");
            return false;
        }
        this.port = Integer.parseInt(port);
        return true;
    }

        private void showToast(String text) {
        Toast.makeText(TCPServerActivity.this, text, Toast.LENGTH_SHORT).show();
    }
}
