package com.example.qidian.protocol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;


public class TCPClientActivity extends AppCompatActivity implements View.OnClickListener,TCPSocket.TCPSocketCallback{
    EditText etAdress, etPort, etContent;
    Button btnStart, btnSend;
    TextView tvSession;
    TCPSocket socket;

    boolean isOpened;
    String ip, content;
    int port;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        setupViews();
    }

    private void setupViews() {
        etAdress = findViewById(R.id.etHost);
        etPort = findViewById(R.id.edit1);
        btnStart = findViewById(R.id.btnOpenClose);
        tvSession = findViewById(R.id.text3);
        btnSend = findViewById(R.id.bottom1);
        etContent = findViewById(R.id.edit2);
        btnStart.setText("开始");
        btnStart.setOnClickListener(this);
        btnSend.setOnClickListener(this);
    }


    @Override
    public void onRead(byte[] data, int length) {
        Log.e("read: ",""+length);

        String rev = null;
        try {
            rev = new String(data,0,length,"utf-8");
            tvSession.append(rev);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSocketState(boolean isOpened) {
        this.isOpened = isOpened;
        if (isOpened){
            btnStart.setText("关闭");
        }else{
            btnStart.setText("开始");
            socket = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (socket != null){
            socket.setSocketCallback(null);
            socket.closeSocket();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOpenClose:
                if(isOpened){
                    isOpened = false;
                    socket.closeSocket();
                    socket = null;
                }else{
                    if (socket == null) {
                        if (checkValidAddress()) {
                            socket = new TCPSocket(ip, port);
                            socket.setSocketCallback(this);
                            socket.openSocket();
                        }
                    }
                }
                break;
            case R.id.bottom1:
                if (isOpened) {
                    String str = etContent.getText().toString();
                    if (!TextUtils.isEmpty(str)) {
                        socket.write(str.getBytes());
                    } else {
                        showToast("请输入发送内容");
                    }
                } else {
                    showToast("请先点击开始再发送");
                }
                break;
            default:
                break;
        }
    }

    private boolean checkValidAddress() {
        String ip = etAdress.getText().toString();
        if (!SocketUtil.isValidHost(ip)) {
            showToast("请输入有效的IP");
            return false;
        }

        String port = etPort.getText().toString();
        if (!SocketUtil.isValidPort(port)) {
            showToast("请输入有效的Port");
            return false;
        }
        this.ip = ip;
        this.port = Integer.parseInt(port);
        return true;
    }

    private void showToast(String text) {
        Toast.makeText(TCPClientActivity.this, text, Toast.LENGTH_SHORT).show();
    }
}
