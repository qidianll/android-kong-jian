package com.example.f;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MyClass {
    private static int i = 0, site, number = -111, start = 1;

    private static String id, outPath;

    private static void readTextFile(String filePath) throws IOException {
        int first = 1;
//        String content = ""; //文件内容字符串
        File file = new File(filePath);
        InputStream instream = null;
        try {
            instream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (instream != null) {
            InputStreamReader inputreader = new InputStreamReader(instream);
            BufferedReader buffreader = new BufferedReader(inputreader);
            String line, data, st;
            //分行读取
            while ((line = buffreader.readLine()) != null) {
                String str = getIdString(line);
                boolean bln = isNumeric(str);
                if (bln && str.length() != 0) {
                    site = Integer.parseInt(str);
                    st = getDataString(line);
                    if (number == site) {
                        i++;
                        String s = st + ",";
                        if (i % 3 == 0) {
                            Write("              " + s);
                        } else {
                            Write(s);
                        }
                    } else {
                        i = 0;
                        number = site;
                        id = "id = " + number + ",";
                        Write(id);
                        data = "data = " + st + ",";
                        Write(data);
                    }
                }
                if (bln && str.length() != 0) {
                    first++;
                }
                if (first == 4) {
                    first = 1;
                    Write("\n");
                }
//                content += line + "\n";
            }
            instream.close();
        }
//        return content;
    }

    public static String getIdString(String str) {
        String s = ",";
        int start = str.indexOf(s);
        int end = str.indexOf(s, start + 1);
        String content = str.substring(start + 1, end);
        return content;
    }

    public static String getDataString(String str) {
        String s = str;
        String st = ",";
        int start = s.indexOf(st, 23);
        int end = s.indexOf(st, 28);
        String content = s.substring(start + 1, end);
        return content;
    }

    public static boolean isNumeric(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static void Write(String str) throws IOException {
        start++;
        String s = str;
        FileOutputStream out = new FileOutputStream(outPath, true);
        byte[] b = s.getBytes();
        out.write(b);
        out.close();
    }

    public static void main(String[] args) {
        String inPath = "/home/qidian/桌面/kkc.txt";
        outPath = "/home/qidian/桌面/qwer.txt";
        File file = new File(outPath);
        if (file.exists()) {
            try {
//                long startTime = System.currentTimeMillis();
//                for (int aaa=1;aaa<1000;aaa++){
                readTextFile(inPath);
//                }
//                long endTime = System.currentTimeMillis();
//                System.out.println("总共发费时间 : " +(endTime-startTime) +"毫秒");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
