package com.example.qidian.module;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;


public class ViewActivity extends AppCompatActivity {
    private RecyclerView recycle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        initView();
        initData();
    }

    private void initView() {
        recycle = (RecyclerView) findViewById(R.id.recycle);
    }

    private void initData() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycle.setLayoutManager(layoutManager );
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        RecycleAdapter recycleAdapter = new RecycleAdapter(this);
        recycle.setAdapter(recycleAdapter);
        recycleAdapter.setOnRecycleClickLisener(new RecycleAdapter.RecycleClick() {
            @Override
            public void onRecycleClick(View v, int position) {
                Toast.makeText(ViewActivity.this, "点击了position="+position, Toast.LENGTH_SHORT).show();
            }
        });

    }
}