package com.example.qidian.module;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class ListViewActivity extends AppCompatActivity {
    private ListView listView;
    private ArrayAdapter<String>arr_aAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        listView =(ListView)findViewById(R.id.listView1);

        String[] arr_data = {"Apple","Banana","Orange","Watermelon","Pear","Grape","Mango","Cherry"};
        arr_aAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arr_data);
        listView.setAdapter(arr_aAdapter);

    }

    public void lla (View view) { startActivity(new Intent(this,Main2Activity.class)); }
    public void li(View view){startActivity(new Intent(this,Main3Activity.class));}
}
