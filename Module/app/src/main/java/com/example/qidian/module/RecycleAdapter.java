package com.example.qidian.module;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

public class RecycleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final String TAG = "RecycleAdapter";
    
    String[] contents = new String[]{"纵向列表","横向列表","纵向网格","横向网格","纵向瀑布流","横向瀑布流"};
    private Context context;
    private AdapterView.OnItemClickListener onItemClickListener;

    public RecycleAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycle_item, parent, false);
        return new RecyclerHolder(view);
   }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        ((RecyclerHolder)holder).content.setText(contents[position]);
        ((RecyclerHolder) holder).setData(position);
    }

    @Override
    public int getItemCount() {
        return contents.length;
    }

    class RecyclerHolder extends RecyclerView.ViewHolder{
        public TextView content;

        public RecyclerHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.tv_recycle);

        }

        public void setData(final int position) {
            content.setText(contents[position]);
            content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recycleClick!=null){
                        recycleClick.onRecycleClick(content,position);
                    }
                }
            });

        }
    }

    public void setOnRecycleClickLisener(RecycleClick rc){
        recycleClick = rc;
    }

    RecycleClick recycleClick;

    public interface RecycleClick{
        void onRecycleClick(View v,int position);
    }
}
