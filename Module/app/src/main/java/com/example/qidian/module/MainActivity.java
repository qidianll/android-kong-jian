package com.example.qidian.module;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    public static final String[] commonFunList = new String[]{
            "Button控件",
            "TextView控件",
            "ImageView控件",
            "EditText控件",
            "ProgressBar控件",
            "SeekBar控件",
            "Switch控件",
            "Spinner控件",
            "LinearLayout",
            "RelativeLayout",
            "FrameLayout",
            "ListView控件",
            "ScrollView控件",
            "Fragment",
            "ConstraintLayout",
            "RecycleView控件",

    };
    private ListView listView;
    private void toNewActivity(int position){
        Intent i;
        switch(position){
            case 0:
                 i = new Intent(MainActivity.this,ButtonActivity.class);
                break;
            case 13:
                 i = new Intent(MainActivity.this,FragmentActivity.class);
                break;
            case 2:
                 i = new Intent(MainActivity.this,ImageViewActivity.class);
                break;
            case 12:
                 i = new Intent(MainActivity.this,ScrollViewActivity.class);
                break;
            case 6:
                 i = new Intent(MainActivity.this,SwitchActivity.class);
                break;
            case 1:
                 i = new Intent(MainActivity.this,TextViewActivity.class);
                break;
            case 3:
                 i = new Intent(MainActivity.this,EditTextActivity.class);
                break;
            case 4:
                 i = new Intent(MainActivity.this,ProgressBarActivity.class);
                break;
            case 5:
                 i = new Intent(MainActivity.this,SeekBarActivity.class);
                 break;
            case 7:
                i = new Intent(MainActivity.this,SpinnerActivity.class);
                break;
            case 8:
                i = new Intent(MainActivity.this,LinearLayoutActivity.class);
                break;
            case 9:
                i = new Intent(MainActivity.this,RelativeLayoutActivity.class);
                break;
            case 10:
                i = new Intent(MainActivity.this,FrameLayoutActivity.class);
                break;
            case 11:
                i = new Intent(MainActivity.this,ListViewActivity.class);
                break;
            case 14:
                i = new Intent(MainActivity.this,ConstraintLayoutActivity.class);
                break;
            case 15:
                i = new Intent(MainActivity.this,ViewActivity.class);
                break;
            default:
                i = new Intent(MainActivity.this,MainActivity.class);
                break;
        }
        startActivity(i);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BlankFragment fragment = new BlankFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.layout,fragment,"abc");
        ft.commit();

        listView = (ListView) findViewById(R.id.listView_fun);
        listView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                commonFunList));
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        toNewActivity(position);
                    }
                }
        );

    }
}
