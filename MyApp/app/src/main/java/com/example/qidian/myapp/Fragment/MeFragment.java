package com.example.qidian.myapp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.sdk.android.feedback.impl.FeedbackAPI;
import com.alibaba.sdk.android.feedback.util.IUnreadCountCallback;
import com.baidu.autoupdatesdk.BDAutoUpdateSDK;
import com.baidu.autoupdatesdk.UICheckUpdateCallback;
import com.example.qidian.myapp.Helper.AppConfig;
import com.example.qidian.myapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeFragment extends Fragment implements View.OnClickListener{

    private  final static String DOWNLOAD_URI = "http://a.app.qq.com/o/simple.jsp?pkgname=cfans.app.qrgen";
    View mView;
    TextView mTvUnread;

    public MeFragment() {
        // Required empty public constructor
    }

    public static MeFragment newInstance() {
        MeFragment fragment = new MeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(null == mView) {
            mView = inflater.inflate(R.layout.fragment_me, container, false);
            setupViews();
        }
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        FeedbackAPI.getFeedbackUnreadCount(new IUnreadCountCallback(){
            @Override
            public void onSuccess(final int i) {
                mTvUnread.post(new Runnable() {
                    @Override
                    public void run() {
                        String text = "";
                        if (i>0){
                            text = String.format(getString(R.string.unread_count), i);
                        }
                        mTvUnread.setText(text);
                        mTvUnread.setVisibility(View.VISIBLE);
                    }
                });
            }
            @Override
            public void onError(int i, String s) {
            }
        });
    }

    private void setupViews() {
        mView.findViewById(R.id.rl_reward).setOnClickListener(this);
        mView.findViewById(R.id.rl_mark).setOnClickListener(this);
        mView.findViewById(R.id.rl_share).setOnClickListener(this);
        mView.findViewById(R.id.rl_clear).setOnClickListener(this);
        mView.findViewById(R.id.rl_version).setOnClickListener(this);
        String version = String.format(getString(R.string.version_value), AppConfig.getVersionName(getContext()));
        ((TextView)mView.findViewById(R.id.tv_version_value)).setText(version);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_mark:
                if (AppConfig.hasAnyMarketInstalled(getContext())){
                    AppConfig.openMarketToMark(getContext());
                }else{
                    AppConfig.showToast(getActivity(),R.string.tip_uninstall_market);
                }
                break;
            case R.id.rl_share:
                AppConfig.systemShareText(getActivity(),getString(R.string.share_app_text)+"\r\n"+DOWNLOAD_URI);
                break;
            case R.id.rl_reward:
                if (!AppConfig.startAlipayClient(getActivity(), "FKX03268HPNHSBDXWPFT96?")){
                    AppConfig.showToast(getActivity(),R.string.tip_a_reward);
                }
                break;
            case R.id.rl_clear:
                AppConfig.cleanApplicationData(getContext(),"");
                AppConfig.showToast(getContext(),getString(R.string.tip_clear_cache));
                break;
            case R.id.rl_version:
                BDAutoUpdateSDK.uiUpdateAction(this.getContext(), new UICheckUpdateCallback() {
                    @Override
                    public void onNoUpdateFound() {
                        AppConfig.showToast(getContext(),getString(R.string.latest_version));
                    }
                    @Override
                    public void onCheckComplete() {

                    }
                });
                break;
            default:
                break;
        }
    }

}
