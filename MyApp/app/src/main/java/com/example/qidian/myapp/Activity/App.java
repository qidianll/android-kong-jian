package com.example.qidian.myapp.Activity;

import android.app.Application;
import android.os.StrictMode;

import com.uuzuche.lib_zxing.activity.ZXingLibrary;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class App extends Application {

    public static List<File> sQRCodeList = new LinkedList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        ZXingLibrary.initDisplayOpinion(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();

    }
}
