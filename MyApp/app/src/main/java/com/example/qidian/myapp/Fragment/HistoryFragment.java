package com.example.qidian.myapp.Fragment;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qidian.myapp.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    ListView listView;
    List<String> mImagePaths;
    MyAdapter adapter;
    Context con;

    public HistoryFragment() {
        // Required empty public constructor
    }

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        con = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (null == listView) {
            listView = (ListView) inflater.inflate(R.layout.fragment_histroy_list, container, false);
            mImagePaths = new ArrayList<>();
            adapter = new MyAdapter();
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent it = new Intent(Intent.ACTION_VIEW);
                    Uri mUri = Uri.parse("file://" + mImagePaths.get(position));
                    it.setDataAndType(mUri, "image/*");
                    startActivity(it);
                }
            });
        }
        return listView;
    }

    public void read() {
        mImagePaths.clear();
        String filePath;
        boolean hasSDCard = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        if (hasSDCard) { // SD卡根目录的hello.text
            filePath = Environment.getExternalStorageDirectory().toString() + File.separator + "MyApp";
        } else  // 系统下载缓存根目录的hello.text
            filePath = Environment.getDownloadCacheDirectory().toString() + File.separator + "MyApp";
        File file = new File(filePath);
        if (file.exists()) {
            File[] files = file.listFiles();
            for (File f : files) {
                String str = f.getPath();
                mImagePaths.add(str);
            }
            adapter.notifyDataSetChanged();
        } else {
            file.mkdir();
            File[] files = file.listFiles();
            for (File f : files) {
                String str = f.getPath();
                mImagePaths.add(str);
            }
            adapter.notifyDataSetChanged();
        }
    }

    class MyHolder {
        ImageView imageView, share, delet;
        TextView textView;
    }

    class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mImagePaths.size();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final MyHolder holder;
            String path = mImagePaths.get(position);
            if (convertView == null) {
                holder = new MyHolder();
                convertView = LayoutInflater.from(con).inflate(R.layout.fragment_history, null);
                holder.imageView = convertView.findViewById(R.id.iv_image);
                holder.textView = convertView.findViewById(R.id.tv_name);
                holder.delet = convertView.findViewById(R.id.iv_delete);
                holder.share = convertView.findViewById(R.id.iv_share);
                holder.delet.setTag(path);
                holder.share.setTag(path);

                convertView.setTag(holder);
            } else {
                holder = (MyHolder) convertView.getTag();
            }
            Bitmap btp = BitmapFactory.decodeFile(path);
            holder.imageView.setImageBitmap(btp);
            File file = new File(path);
            holder.textView.setText(file.getName());
            holder.delet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String path = (String) v.getTag();
                    if (new File(path).delete()) {
                        mImagePaths.remove(path);
                    }
                    listView.setAdapter(adapter);
                }
            });
            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri;
                    String path = (String)v.getTag();
                    File file = new File(path);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                        uri = Uri.fromFile(file);
                    } else {
                        uri = FileProvider.getUriForFile(con, con.getPackageName(), file);
                    }
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("image/*");
                    sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    con.startActivity(sendIntent);
                }
            });
            return convertView;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}
