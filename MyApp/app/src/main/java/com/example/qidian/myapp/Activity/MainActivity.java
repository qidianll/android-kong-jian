package com.example.qidian.myapp.Activity;

import android.content.DialogInterface;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.example.qidian.myapp.Adapter.ViewPagerAdapter;
import com.example.qidian.myapp.Fragment.BuildFragment;
import com.example.qidian.myapp.Fragment.HistoryFragment;
import com.example.qidian.myapp.Helper.NoScrollViewPager;
import com.example.qidian.myapp.R;
import com.example.qidian.myapp.Helper.BottomNavigationViewHelper;
import com.example.qidian.myapp.Fragment.MeFragment;
import java.io.File;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    BottomNavigationView bottomNavigationView;
    HistoryFragment historyFragment;
    MeFragment meFragment;
    BuildFragment buildFragment;
    MenuItem menuItem, clear;
    AlertDialog mDeleteDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (NoScrollViewPager) findViewById(R.id.viewpager);
        bottomNavigationView = findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_build:
                        viewPager.setCurrentItem(0);
                        clear.setVisible(false);
                        break;
                    case R.id.navigation_history:
                        viewPager.setCurrentItem(1);
                        clear.setVisible(true);
                        historyFragment.read();
                        break;
                    case R.id.navigation_me:
                        clear.setVisible(false);
                        viewPager.setCurrentItem(2);
                        break;
                }
                return false;
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (menuItem != null) {
                    menuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                menuItem = bottomNavigationView.getMenu().getItem(position);
                menuItem.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        buildFragment = BuildFragment.newInstance();
        adapter.addFragment(buildFragment);
        historyFragment = HistoryFragment.newInstance();
        adapter.addFragment(historyFragment);
        meFragment = MeFragment.newInstance();
        adapter.addFragment(meFragment);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu_sc, menu);
        clear = menu.findItem(R.id.navigation_top_clear);
        clear.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (viewPager.getCurrentItem() == 1) {
            if (item.getItemId() == R.id.navigation_top_clear) {
                onClearAllRecords();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void onClearAllRecords() {
        if (mDeleteDialog == null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("确定删除所有记录?");
            alertDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String filePath;
                    boolean hasSDCard = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
                    if (hasSDCard) { // SD卡根目录的hello.text
                        filePath = Environment.getExternalStorageDirectory().toString() + File.separator + "MyApp";
                    } else  // 系统下载缓存根目录的hello.text
                        filePath = Environment.getDownloadCacheDirectory().toString() + File.separator + "MyApp";
                    File file = new File(filePath);
                    File[] files = file.listFiles();
                    for (File f : files) {
                        f.delete();
                    }
                    historyFragment.read();
                }
            });
            alertDialog.setNegativeButton("取消", null);
            mDeleteDialog = alertDialog.create();
        }
        mDeleteDialog.show();
    }
}
