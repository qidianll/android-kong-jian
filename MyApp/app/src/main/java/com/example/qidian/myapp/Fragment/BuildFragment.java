package com.example.qidian.myapp.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.example.qidian.myapp.Activity.IDActivity;
import com.example.qidian.myapp.Activity.PhoneActivity;
import com.example.qidian.myapp.Activity.ScanActivity;
import com.example.qidian.myapp.Activity.TextActivity;
import com.example.qidian.myapp.Activity.UirActivity;
import com.example.qidian.myapp.R;

import org.w3c.dom.Text;

import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class BuildFragment extends Fragment {

    View mView;
    Button id, ur, ph, te, scan;

    public BuildFragment() {
        // Required empty public constructor
    }

    public static BuildFragment newInstance() {
        BuildFragment fragment = new BuildFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_build, container, false);
            id = mView.findViewById(R.id.id);
            ur = mView.findViewById(R.id.url);
            ph = mView.findViewById(R.id.phone);
            te = mView.findViewById(R.id.text);
            scan = mView.findViewById(R.id.scan);

            View.OnClickListener listener = null;
            listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.id:
                            Intent intent = new Intent(getActivity(), IDActivity.class);
                            startActivity(intent);
                            break;
                        case R.id.phone:
                            intent = new Intent(getActivity(), PhoneActivity.class);
                            startActivity(intent);
                            break;
                        case R.id.url:
                            intent = new Intent(getActivity(), UirActivity.class);
                            startActivity(intent);
                            break;
                        case R.id.text:
                            intent = new Intent(getActivity(), TextActivity.class);
                            startActivity(intent);
                            break;
                        case R.id.scan:
                            intent = new Intent(getActivity(), ScanActivity.class);
                            startActivity(intent);

                    }
                }
            };
            id.setOnClickListener(listener);
            ur.setOnClickListener(listener);
            te.setOnClickListener(listener);
            ph.setOnClickListener(listener);
            scan.setOnClickListener(listener);
        }
        return mView;
    }
}
