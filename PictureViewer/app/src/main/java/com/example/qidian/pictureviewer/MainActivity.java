package com.example.qidian.pictureviewer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editText;
    List<String> mImagePaths;
    private static final String TAG = "MainActivity";
    MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BlankFragment fragment = new BlankFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ff = manager.beginTransaction();
        ff.replace(R.id.layout, fragment, "a");
        ff.commit();

        editText = findViewById(R.id.editText);
        ListView listView = findViewById(R.id.listView);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(this);
        mImagePaths = new ArrayList<>();
        adapter = new MyAdapter();
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                String filePath = Environment.getExternalStorageDirectory() + editText.getText().toString();
                Log.e(TAG, "onClick: " + filePath);
                File file = new File(filePath);
                if (file.exists()) {
                    if (file.isDirectory()) {
                        File[] files = file.listFiles();
                        for (File f : files) {
                            String str = f.getPath();
                            mImagePaths.add(str);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    class MyHolder {
        ImageView imageView;
    }


    class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mImagePaths.size();

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MyHolder holder;
            if (convertView == null) {
                holder = new MyHolder();
                convertView = LayoutInflater.from(MainActivity.this).inflate(R.layout.listview_item, null);
                holder.imageView = convertView.findViewById(R.id.imageView);
                convertView.setTag(holder);
            } else {
                holder = (MyHolder) convertView.getTag();
            }

//            Bitmap bitmap =  BitmapFactory.decodeFile(mImagePaths.get(position));
//            holder.imageView.setImageBitmap(bitmap);
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage("file://" + mImagePaths.get(position), holder.imageView);


            return convertView;
        }


        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
    }
}
