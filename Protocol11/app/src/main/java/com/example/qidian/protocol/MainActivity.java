package com.example.qidian.protocol;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void aa(View view){
        startActivity(new Intent(this, Main2Activity.class));
    }
    public void bb(View view){
        startActivity(new Intent(this, Main3Activity.class));
    }
    public void cc(View view){
        startActivity(new Intent(this, Main4Activity.class));
    }
    public void dd(View view){
        startActivity(new Intent(this, Main5Activity.class));
    }
    public void ee(View view){
        startActivity(new Intent(this, Main6Activity.class));
    }
}
