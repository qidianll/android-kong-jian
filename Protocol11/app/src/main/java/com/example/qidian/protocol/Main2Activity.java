package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {
    EditText mtext, mtext1, mtext2;
    Button button, button1;
    TextView textView;
    boolean aBoolean ;
    String aa, str;
    int port;
    Socket s;

    Handler handler = new Handler();
    public class ReceiveThread implements Runnable {
        public void run() {
            try {
                s = new Socket(aa, port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(s!=null) {
                while (s.isConnected()) {
                    byte b[] = new byte[1024];
                    InputStream inputStream = null;
                    try {
                        inputStream = s.getInputStream();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        inputStream.read(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    final String kk = new String(b);
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("接受：" + kk + "\n");
                        }
                    });
                }
            }else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                Toast.makeText(Main2Activity.this, "未连接到服务端", Toast.LENGTH_SHORT).show();
            }
                });
            }
        }
    }
    public class SendThread implements Runnable{
        public void run() {
            OutputStream outputStream = null;
                try {
                    outputStream = s.getOutputStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    outputStream.write(str.getBytes());
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("发送：" + str + "\n");
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mtext = findViewById(R.id.edit);
        mtext1 = findViewById(R.id.edit1);
        button = findViewById(R.id.bottom);
        textView = findViewById(R.id.text3);
        button1 = findViewById(R.id.bottom1);
        mtext2 = findViewById(R.id.edit2);
        button.setText("开始");
        button.setOnClickListener(this);
        button1.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (s!=null){
            try {
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {

        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bottom:
                aBoolean = !aBoolean;
                if (aBoolean == true) {
                    button.setText("取消");
                } else {
                    button.setText("开始");
                }
                String ip = mtext.getText().toString();
                if (ip.length() < 16 && ip.length() > 6) {
                    aa = ip;
                    String pp = mtext1.getText().toString();
                    Pattern pattern = Pattern.compile("[0-9]*");
                    Matcher matcher = pattern.matcher(pp);
                    if (matcher.matches()) {
                        port = Integer.parseInt(pp);
                        new Thread(new ReceiveThread()).start();
                    } else {
                        Toast.makeText(Main2Activity.this, "端口错误", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Main2Activity.this, "IP错误", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.bottom1:
                if(aBoolean){
                    str = mtext2.getText().toString();
                    new Thread(new SendThread()).start();
                }else {
                    Toast.makeText(Main2Activity.this,"请确定连接到服务端",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
