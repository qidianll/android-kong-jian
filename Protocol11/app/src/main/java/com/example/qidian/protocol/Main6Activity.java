package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main6Activity extends AppCompatActivity implements View.OnClickListener{

    EditText mtext, mtext2;
    Button button, button1;
    TextView textView;
    boolean aBoolean;
    int portS ,portC;
    String str,line;
    DatagramSocket ds = null;
    DatagramPacket dp;
    byte[] buf;

    public class Receive implements Runnable{
        public void run() {
            try{
                while(aBoolean){
                    buf = new byte[1024];
                    dp = new DatagramPacket(buf,buf.length);
                    ds.receive(dp);
                    final String ip = dp.getAddress().getHostAddress();
                    line = ip;
                    final String data = new String(dp.getData(),0,dp.getLength());
                    portC=dp.getPort();
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("ip地址："+ip+" 端口号："+portC+" 消息："+data+"\n");
                        }
                    });
                }
            }catch(Exception e){
                throw new RuntimeException("接收失败");
            }
        }
    }


    Handler handler = new Handler();
    private static final String TAG = "Main6Activity";

    public class Send implements Runnable{
        public void run() {
            try{
                if (aBoolean&&str.length()!=0) {
                        byte[] b = str.getBytes();
                    Log.e(TAG, "run: "+line);
                        DatagramPacket dd = new DatagramPacket(b, b.length,InetAddress.getByName(line), portC);
                        ds.send(dd);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                textView.append(str + "\n");
                            }
                        });
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        mtext = findViewById(R.id.edit);
        button = findViewById(R.id.bottom);
        textView = findViewById(R.id.text3);
        button1 = findViewById(R.id.bottom1);
        mtext2 = findViewById(R.id.edit2);
        button.setText("开始");
        button.setOnClickListener(this);
        button1.setOnClickListener(this);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bottom:
                aBoolean = !aBoolean;
                if (aBoolean == true) {
                    button.setText("取消");
                } else {
                    button.setText("开始");
                }
                String pp = mtext.getText().toString();
                Pattern pattern = Pattern.compile("[0-9]*");
                Matcher matcher = pattern.matcher(pp);
                if (matcher.matches()) {
                    portS = Integer.parseInt(pp);
                    try {
                        ds = new DatagramSocket(portS);
                    } catch (SocketException e) {
                        e.printStackTrace();
                    }
                    new Thread(new Receive()).start();
                } else {
                    Toast.makeText(Main6Activity.this, "请输入数字", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.bottom1:
                if(aBoolean){
                    str = mtext2.getText().toString();
                        new Thread(new Send()).start();
                }else {
                    Toast.makeText(Main6Activity.this, "请点击开始", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
