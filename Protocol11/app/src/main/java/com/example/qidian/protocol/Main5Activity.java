package com.example.qidian.protocol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main5Activity extends AppCompatActivity implements View.OnClickListener{

    EditText mtext, mtext2;
    Button button, button1;
    TextView textView;
    boolean aBoolean;
    String str;
    int port;
    Socket s = null;
    ServerSocket ss;
    public static List<Object> socketList= Collections.synchronizedList(new ArrayList<>());

    public class SendThread implements Runnable{
        Socket s;
        OutputStream outputStream;
        public SendThread(Socket s)throws IOException{
            this.s = s;
            outputStream = s.getOutputStream();
        }
        public void run() {
            try {
                if(s.isConnected()) {
                    byte[] out = str.getBytes();
                    outputStream.write(out, 0, out.length);
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("发送消息为：" + str + "\n");
                        }
                    });
                }else {
                    Toast.makeText(Main5Activity.this,"等待连接",Toast.LENGTH_SHORT).show();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            socketList.remove(this.s);
        }
    }

    public class ReceiverThread implements Runnable{
        Socket s;
        InputStream inputStream;
        public ReceiverThread(Socket s)throws IOException{
            this.s = s;
            inputStream = s.getInputStream();
        }
        public void run() {
            try {
                String content ;
                byte[] recv = new byte[1024];
                while(s.isConnected()) {
                    int len = inputStream.read(recv, 0, recv.length);
                    content = new String(recv,0,len);
                    final String finalContent = content;
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("接受消息为：" + finalContent + "\n");
                        }
                    });
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        mtext = findViewById(R.id.edit);
        button = findViewById(R.id.bottom);
        textView = findViewById(R.id.text3);
        button1 = findViewById(R.id.bottom1);
        mtext2 = findViewById(R.id.edit2);
        button.setText("开始");
        button.setOnClickListener(this);
        button1.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bottom:
                aBoolean = !aBoolean;
                if (aBoolean == true) {
                    button.setText("取消");
                } else {
                    button.setText("开始");
                }
                String pp = mtext.getText().toString();
                Pattern pattern = Pattern.compile("[0-9]*");
                Matcher matcher = pattern.matcher(pp);
                if (matcher.matches()) {
                    port = Integer.parseInt(pp);
                    new Thread(new kk()).start();
                } else {
                        Toast.makeText(Main5Activity.this, "请输入数字", Toast.LENGTH_SHORT).show();
                    }
                break;
            case R.id.bottom1:
                if(aBoolean){
                str = mtext2.getText().toString();
                if(str.length() != 0){
                    try {
                        new Thread(new SendThread(s)).start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(Main5Activity.this, "请输入内容", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(Main5Activity.this, "请点击开始", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public class kk implements Runnable {
        public void run() {
            try {
                ss = new ServerSocket(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                s =ss.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            socketList.add(s);
            try {
                new Thread(new ReceiverThread(s)).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (s!=null){
            try {
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {

        }
    }
}
