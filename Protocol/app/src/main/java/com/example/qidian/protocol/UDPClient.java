package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UDPClient extends AppCompatActivity implements View.OnClickListener {
    EditText mtext ,mtext1,mtext2;
    Button button,button1;
    TextView textView;
    String aa;
    InputStream bb;
    int port;
    String str;
    boolean aBoolean;
    DatagramSocket sendSocket ;

    public class Receive implements Runnable{
        public void run() {
            try{
                while(aBoolean){
                    byte[] buf = new byte[1024];
                    DatagramPacket dp = new DatagramPacket(buf,buf.length);
                    sendSocket.receive(dp);
                    final String ip = dp.getAddress().getHostAddress();
                    final String data = new String(dp.getData(),0,dp.getLength());
                    final int port=dp.getPort();
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("ip地址："+ip+" 端口号："+port+" 消息："+data+"\n");
                        }
                    });
                }
            }catch(Exception e){
                throw new RuntimeException("接收失败");
            }
        }
    }

    Handler handler = new Handler();

    public class Send implements Runnable{
        public void run() {
            try{
                BufferedReader buf = new BufferedReader(new InputStreamReader(bb));
                String line ;
                if (aBoolean) {
                    while ((line = buf.readLine()) != null) {
                        if ("886".endsWith(line))
                            break;
                        byte[] b = line.getBytes();
                        DatagramPacket dp = new DatagramPacket(b, b.length, InetAddress.getByName(aa), port);
                        sendSocket.send(dp);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                textView.append("发送:"+str + "\n");
                            }
                        });
                    }
                }
            }catch(Exception e){
                throw new RuntimeException("发送失败");
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        setupViews();
    }

    private void setupViews(){
        mtext = findViewById(R.id.edit);
        mtext1 = findViewById(R.id.edit1);
        button = findViewById(R.id.bottom);
        textView = findViewById(R.id.text3);
        button1= findViewById(R.id.bottom1);
        mtext2 = findViewById(R.id.edit2);
        button.setText("开始");
        button.setOnClickListener(this);
        button1.setOnClickListener(this);
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.bottom:
                aBoolean=!aBoolean;
                if(aBoolean == true){
                    button.setText("取消");
                }else {
                    button.setText("开始");
                }
                String ip = mtext.getText().toString();
                if (ip.length()<16&&ip.length()>6){
                    aa=ip;
                    String pp = mtext1.getText().toString();
                    Pattern pattern = Pattern.compile("[0-9]*");
                    Matcher matcher =pattern.matcher(pp);
                    if (matcher.matches()){
                        port = Integer.parseInt(pp);
                        try {
                            sendSocket = new DatagramSocket(port);
                        } catch (SocketException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        new Thread(new Receive()).start();
                    }else {
                        Toast.makeText(UDPClient.this,"端口错误",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(UDPClient.this,"IP错误",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.bottom1:
                str = mtext2.getText().toString();
                bb =new ByteArrayInputStream(str.getBytes());
                String pp = mtext1.getText().toString();
                Pattern pattern = Pattern.compile("[0-9]*");
                Matcher matcher =pattern.matcher(pp);
                if (matcher.matches()){
                    port = Integer.parseInt(pp);
                    new Thread(new Send()).start();
                }
                break;
        }
    }
}
