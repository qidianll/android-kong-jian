package com.example.qidian.protocol;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPBroadcastServer extends AppCompatActivity implements View.OnClickListener{

    EditText mtext, mtext2;
    Button button, button1;
    TextView textView;
    boolean aBoolean;
    int portC,portS;
    String str,line;
    DatagramSocket socket;
    DatagramPacket packet;
    byte[] buf;

    public class Receive implements Runnable{
        public void run() {
            try{
                while(aBoolean){
                    buf = new byte[1024];
                    packet = new DatagramPacket(buf,buf.length);
                    socket.receive(packet);
                    final String ip = packet.getAddress().getHostAddress();
                    line = ip;
                    final String data = new String(packet.getData(),0,packet.getLength());
                    portC=packet.getPort();
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("ip地址："+ip+"端口号："+portC+"接收消息为："+data+"\n");
                        }
                    });
                }
            }catch(Exception e){
                throw new RuntimeException("接收失败");
            }
        }
    }


    Handler handler = new Handler();

    public class Send implements Runnable{
        public void run() {
            try{
                if (aBoolean&&str.length()!=0) {
                    byte[] b = str.getBytes();
                    DatagramPacket dd = new DatagramPacket(b, b.length,InetAddress.getByName("255.255.255.255"),portS);
                    socket.send(dd);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.append("发送消息为:"+str + "\n");
                        }
                    });
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_udpmulticast);

        mtext = findViewById(R.id.edit);
        button = findViewById(R.id.bottom);
        textView = findViewById(R.id.text3);
        button1 = findViewById(R.id.bottom1);
        mtext2 = findViewById(R.id.edit2);
        button.setText("开始");
        button.setOnClickListener(this);
        button1.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bottom:
                aBoolean = !aBoolean;
                if (aBoolean == true) {
                    button.setText("取消");
                } else {
                    button.setText("开始");
                }
                String pp = mtext.getText().toString();
                portS = Integer.parseInt(pp);
                try {
                    socket = new DatagramSocket();
                } catch (SocketException e) {
                    e.printStackTrace();
                }
                new Thread(new Receive()).start();
                break;
            case R.id.bottom1:
                if (aBoolean) {
                    str = mtext2.getText().toString();
                    new Thread(new Send()).start();
                } else {
                    Toast.makeText(UDPBroadcastServer.this, "请点击开始", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (socket!=null){
//            socket.close();
//        }
//    }
}