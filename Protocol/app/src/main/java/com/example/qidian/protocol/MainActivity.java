package com.example.qidian.protocol;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void aa(View view){
        startActivity(new Intent(this, TCPClient.class));
    }
    public void bb(View view){
        startActivity(new Intent(this, UDPClient.class));
    }
    public void dd(View view){
        startActivity(new Intent(this, TCPServer.class));
    }
    public void ee(View view){
        startActivity(new Intent(this, UDPServer.class));
    }
    public void kk(View view){
        startActivity(new Intent(this, UDPBroadcastServer.class));
    }
    public void cc(View view){
        startActivity(new Intent(this, UDPMulticast.class));
    }
    public void ll(View view){
        startActivity(new Intent(this, UDPBroadcastClient.class));
    }
}
