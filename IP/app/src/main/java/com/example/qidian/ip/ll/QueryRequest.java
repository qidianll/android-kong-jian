package com.example.qidian.ip.ll;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cfans on 2017/7/3.
 */

public class QueryRequest extends JsonRequest<QueryResult> {
    private final static String AppCode = "174d1efb4b8a4c5f8a164357510db465";
    private final static String ALI_URL = "https://dm-81.data.aliyun.com/rest/160601/ip/getIpInfo.json?ip=%s";


    private final static int MONTH = 365*24*60*60*1000;
    private static Gson sGson = new Gson();


    public QueryRequest(int method, String url, String requestBody, Response.Listener<QueryResult> listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        Cache.Entry entry = new Cache.Entry();
        entry.ttl = System.currentTimeMillis()+MONTH;
        setCacheEntry(entry);
        setShouldCache(true);
    }

    @Override
    protected Response<QueryResult> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(sGson.fromJson(jsonString, QueryResult.class),
                    HttpHeaderParser.parseCacheHeaders(response));//用Gson解析返回Java对象
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }catch (NullPointerException e){
            return Response.error(new ParseError(e));
        }
    }

    public QueryRequest(String number, Response.Listener<QueryResult> listener, Response.ErrorListener errorListener) {
        this(Request.Method.GET,String.format(ALI_URL,number),null, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "APPCODE " + AppCode);
        return headers;
    }


    @Override
    public void deliverError(VolleyError error) {

        if (error instanceof NoConnectionError) {
            QueryResult result = getCachedResult();
            if (result != null){
                deliverResponse(result);
                return;
            }
        }
        super.deliverError(error);
    }


    private QueryResult getCachedResult(){
        Cache.Entry entry = this.getCacheEntry();
        if (entry != null) {
            // 解析entry 并封装成response
            Response<QueryResult> response = parseNetworkResponse(new NetworkResponse(
                    entry.data, entry.responseHeaders));
            return response.result;
        }
        return null;
    }

    public QueryResult getCachedResult(RequestQueue requestQueue){
        Cache.Entry entry = requestQueue.getCache().get(getUrl());
        if (entry != null) {
            // 解析entry 并封装成response
            Response<QueryResult> response = parseNetworkResponse(new NetworkResponse(
                    entry.data, entry.responseHeaders));
            return response.result;
        }
        return null;
    }
}
