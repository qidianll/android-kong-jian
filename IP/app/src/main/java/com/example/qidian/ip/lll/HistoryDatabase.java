package com.example.qidian.ip.lll;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class HistoryDatabase {

	public static final String TABLE_NAME = "history_tb";
	public static final String COL_PHONE = "phone";
	public static final String COL_LAST_TIME = "last_access_time";

	private static HistoryDatabase sInstance;
	private DBOpenHelper mDbOpenHelper;
	private ReentrantLock mLock;

	private HistoryDatabase(Context ctx) {
		if (mDbOpenHelper == null) {
			mDbOpenHelper = DBOpenHelper.getInstance(ctx);
			mLock = mDbOpenHelper.getLock();
		}
	}

	public static HistoryDatabase shareInstance(Context ctx) {
		if (sInstance == null) {
			sInstance = new HistoryDatabase(ctx);
		}
		return sInstance;
	}


	public boolean insert(String number) {
		if (number == null) {
			return false;
		}else if (isExist(number)){
			return update(number);
		}

		SQLiteDatabase db = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getWritableDatabase();
			ContentValues values = createValues(number);
			long ret = db.insert(TABLE_NAME, null, values);
			return ret != -1;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}

		return false;
	}

	public boolean delete(String id) {
		SQLiteDatabase db = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getWritableDatabase();
			String where = COL_PHONE + "=?";
			String[] whereValue = { id};
			long ret = db.delete(TABLE_NAME, where, whereValue);
			return ret != -1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return false;
	}

	public boolean isExist(String id){
		if (TextUtils.isEmpty(id)) {
			throw new IllegalStateException(
					"id must not be null and size must > zero");
		}
		SQLiteDatabase db = null;
		Cursor c = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getReadableDatabase();
			String[] columns = new String[] { COL_PHONE };
			String selection = COL_PHONE + "=?";
			String[] selectionArgs = new String[] { id };
			c = db.query(TABLE_NAME, columns, selection, selectionArgs, null,
					null, null);
			if (c.moveToFirst()) {
				return true;
			}
			return false;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null) {
				c.close();
			}
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return false;
	}


	public boolean update(String number) {
		SQLiteDatabase db = null;
		try {
			mLock.lock();
			db = mDbOpenHelper.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(COL_LAST_TIME, System.currentTimeMillis()/1000);
			String whereClause = COL_PHONE + "=?";
			String[] whereArgs = new String[] { number};
			long ret = db.update(TABLE_NAME, values, whereClause, whereArgs);
			return ret != 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}

		return false;
	}


	public ArrayList<String> readAll() {
		ArrayList<String> camList = new ArrayList<String>();
		Cursor c = null;
		SQLiteDatabase db = null;

		try {
			mLock.lock();
			db = mDbOpenHelper.getReadableDatabase();
			c = db.query(TABLE_NAME, null, null, null, null, null, COL_LAST_TIME+" ASC");
			for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
				String model = createCamearaDBModel(c);
				camList.add(model);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (c != null) {
				c.close();
			}
			if (db != null) {
				db.close();
			}
			mLock.unlock();
		}
		return camList;
	}

	private String createCamearaDBModel(Cursor c) {
		String id = c.getString(c.getColumnIndex(COL_PHONE));
		return id;
	}

	private ContentValues createValues(String cam) {
		ContentValues values = new ContentValues();
		values.put(COL_PHONE, cam);
		values.put(COL_LAST_TIME, System.currentTimeMillis()/1000);
		return values;
	}

}
