package com.example.qidian.ip.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.qidian.ip.R;
import com.example.qidian.ip.ll.AppConfig;
import com.example.qidian.ip.ll.QueryResult;


public class ResultActivity extends AppCompatActivity {

    public final static String RESULT = "result";

    QueryResult mResult;
    TextView mTvIp, mTvCity, mTvRegion, mTvArea, mTvCountry,mTvIsp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        mResult = (QueryResult) getIntent().getSerializableExtra(RESULT);
        setupViews();
    }

    private void setupHomeButton() {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
//            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    private void setupViews() {

        setupHomeButton();
        mTvIp = findViewById(R.id.tv_ip);
        mTvCity = findViewById(R.id.tv_city);
        mTvRegion = findViewById(R.id.tv_region);
        mTvArea = findViewById(R.id.tv_area);
        mTvCountry = findViewById(R.id.tv_country);
        mTvIsp = findViewById(R.id.tv_isp);

        mTvIp.setText(String.format(getString(R.string.format_ip),mResult.getIp()));
        mTvCity.setText(String.format(getString(R.string.format_city),mResult.getCity()));
        mTvRegion.setText(String.format(getString(R.string.format_region),mResult.getRegion()));
        mTvArea.setText(String.format(getString(R.string.format_area),mResult.getArea()));
        mTvCountry.setText(String.format(getString(R.string.format_contry),mResult.getCounty()));
        mTvIsp.setText(String.format(getString(R.string.format_isp),mResult.getIsp()));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_menu_result,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.navigation_top_share){
            StringBuffer text = new StringBuffer();
            text.append(mTvIp.getText()+"\r\n");
            text.append(mTvCity.getText()+"\r\n");
            text.append(mTvRegion.getText()+"\r\n");
            text.append(mTvArea.getText()+"\r\n");
            text.append(mTvCountry.getText()+"\r\n");
            AppConfig.systemShareText(this,text.toString());
        }else{
            finish();
        }
        return true;
    }
}
