package com.example.qidian.ip.Fragment;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.qidian.ip.Adapter.RecordRecyclerViewAdapter;
import com.example.qidian.ip.lll.App;
import com.example.qidian.ip.R;


public class HistoryFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    RecyclerView mRecyclerView;
    boolean mUIRefresh;

    public HistoryFragment() {
    }

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume"," onResume : "+mUIRefresh);
        if (mUIRefresh){
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(null == mRecyclerView){
            mRecyclerView = (RecyclerView)inflater.inflate(R.layout.fragment_history, container, false);
            setupRecyclerView();
        }
        return mRecyclerView;
    }

    private void setupRecyclerView() {
        Context context = mRecyclerView.getContext();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
            }

            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.bottom = 1;
            }
        });
        mRecyclerView.setAdapter(new RecordRecyclerViewAdapter(App.sHistoryList, mListener));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void refreshUI(boolean now){
        if (now){
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }else{
            mUIRefresh = true;
        }
    }

    public interface OnListFragmentInteractionListener {
        void onItemDeleted(String item);
        void onItemClicked(String item);
    }
}
