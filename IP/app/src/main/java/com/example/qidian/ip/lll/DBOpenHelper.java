package com.example.qidian.ip.lll;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.concurrent.locks.ReentrantLock;

public class DBOpenHelper extends SQLiteOpenHelper {

	/**
	 * 数据库名
	 */
	private static final String DATABASE_NAME = "history_db";

	/**
	 * 数据库版本
	 */
	private static final int DATABASE_VERSION = 1;

	private static DBOpenHelper sInstance;

	private final ReentrantLock mReentrantLock = new ReentrantLock(true);

	private SQLiteDatabase mDatabase;

	public ReentrantLock getLock() {
		return mReentrantLock;
	}

	/**
	 * 获取DBOpenHelper的实例
	 * 
	 * @param context
	 * @return
	 */
	public static synchronized DBOpenHelper getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new DBOpenHelper(context.getApplicationContext());
		}
		return sInstance;
	}

	public SQLiteDatabase getDatabase() {
		if (mDatabase == null || !mDatabase.isOpen()) {
			mDatabase = getWritableDatabase();
		}
		return mDatabase;
	}

	private DBOpenHelper(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createHistoryDatabase(db);
	}

	private void createHistoryDatabase(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + HistoryDatabase.TABLE_NAME
				+ " (" + HistoryDatabase.COL_PHONE + " text primary key, "+HistoryDatabase.COL_LAST_TIME+ " integer"+ ")");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql = "DROP TABLE IF EXISTS " +  HistoryDatabase.TABLE_NAME;
		mDatabase.execSQL(sql);
		onCreate(mDatabase);
	}

}
