package com.example.qidian.ip.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.qidian.ip.Fragment.HistoryFragment;
import com.example.qidian.ip.R;

import java.util.List;


public class RecordRecyclerViewAdapter extends RecyclerView.Adapter<RecordRecyclerViewAdapter.ViewHolder> {

    private final List<String> mValues;
    private final HistoryFragment.OnListFragmentInteractionListener mListener;

    public RecordRecyclerViewAdapter(List<String> items, HistoryFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

//    public void addItem(int position,String file) {
//        mValues.add(position, file);
//        notifyItemInserted(position);
//    }

    public void removeItem(int position) {
            mValues.remove(position);
            notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final String file = holder.mItem = mValues.get(position);
        holder.mTvName.setText(file);
        holder.mIvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    removeItem(position);
                    mListener.onItemDeleted(file);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mIvDelete;
        public final TextView mTvName;
        public String mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIvDelete = (ImageView)view.findViewById(R.id.iv_delete);
            mTvName = (TextView) view.findViewById(R.id.tv_region);
            mView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    if (mListener != null){
                        mListener.onItemClicked(mValues.get(getLayoutPosition()));
                    }
                }
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }
}
