package com.example.qidian.ip.ll;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by cfans on 2017/7/4.
 */

public class QueryResult extends JSONObject implements Serializable {

    public int code;
    public SubResult data;

    public int getCode(){
        return code;
    }

    public String getIp(){
        if (data != null && data.ip !=null){
            return data.ip;
        }else {
            return "";
        }
    }
    public String getArea(){
        if (data != null && data.area !=null){
            return data.area;
        }else {
            return "";
        }
    }

    public String getRegion(){
        if (data != null && data.region !=null){
            return data.region;
        }else {
            return "";
        }
    }
    public String getCity(){
        if (data != null && data.city !=null){
            return data.city;
        }else {
            return "";
        }
    }

    public String getCounty(){
        if (data != null && data.country !=null){
            return data.country;
        }else {
            return "";
        }
    }

    public String getIsp(){
        if (data != null && data.isp !=null){
            return data.isp;
        }else {
            return "";
        }
    }


    public QueryResult(String json) throws JSONException {
        super(json);
    }


    @Override
    public String toString() {
        return "QueryResult{" +
                "code=" + code +
                ", data=" + data +
                '}';
    }

    class SubResult implements Serializable {

        String ip;
        String country;
        String area;
        String region;
        String city;
        String county;
        String isp;
        String country_id;
        String area_id;
        String region_id;
        String city_id;
        String county_id;
        String isp_id;

        @Override
        public String toString() {
            return "SubResult{" +
                    "ip='" + ip + '\'' +
                    ", country='" + country + '\'' +
                    ", area='" + area + '\'' +
                    ", region='" + region + '\'' +
                    ", city='" + city + '\'' +
                    ", county='" + county + '\'' +
                    ", isp='" + isp + '\'' +
                    ", country_id=" + country_id +
                    ", area_id=" + area_id +
                    ", region_id=" + region_id +
                    ", city_id=" + city_id +
                    ", county_id=" + county_id +
                    ", isp_id=" + isp_id +
                    '}';
        }
    }
}
