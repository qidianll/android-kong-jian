package com.example.qidian.ip.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.alibaba.sdk.android.feedback.impl.FeedbackAPI;
import com.alibaba.sdk.android.feedback.util.IUnreadCountCallback;
import com.baidu.autoupdatesdk.BDAutoUpdateSDK;
import com.baidu.autoupdatesdk.UICheckUpdateCallback;
import com.example.qidian.ip.R;
import com.example.qidian.ip.ll.AppConfig;


public class MeFragment extends Fragment implements View.OnClickListener {
    View mView;
    TextView mTvUnread;

    public MeFragment() {
        // Required empty public constructor
    }

    public static MeFragment newInstance() {
        MeFragment fragment = new MeFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(null == mView){
            mView = inflater.inflate(R.layout.fragment_me, container, false);
            setupViews();
        }
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        FeedbackAPI.getFeedbackUnreadCount(new IUnreadCountCallback(){
            @Override
            public void onSuccess(final int i) {
                mTvUnread.post(new Runnable() {
                    @Override
                    public void run() {
                        String text = "";
                        if (i>0){
                            text = String.format(getString(R.string.unread_count), i);
                        }
                        mTvUnread.setText(text);
                        mTvUnread.setVisibility(View.VISIBLE);
                    }
                });
            }
            @Override
            public void onError(int i, String s) {
            }
        });
    }

    private void setupViews() {
        mView.findViewById(R.id.rl_feedback).setOnClickListener(this);
        mView.findViewById(R.id.rl_reward).setOnClickListener(this);
        mView.findViewById(R.id.rl_share).setOnClickListener(this);
        mView.findViewById(R.id.rl_clear).setOnClickListener(this);
        mView.findViewById(R.id.rl_version).setOnClickListener(this);
        String version = String.format(getString(R.string.version_value), AppConfig.getVersionName(getContext()));
        ((TextView)mView.findViewById(R.id.tv_version_value)).setText(version);
        mTvUnread = mView.findViewById(R.id.tv_unread);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_reward:
                if (!AppConfig.startAlipayClient(getActivity(), "FKX05339YNC3FWMTAIZKC4")){
                    AppConfig.showToast(getActivity(),R.string.tip_a_reward);
                }
                break;
            case R.id.rl_feedback:
                FeedbackAPI.openFeedbackActivity();
                break;
            case R.id.rl_share:
                AppConfig.systemShareText(getActivity(),getString(R.string.share_app_text));
                break;
            case R.id.rl_clear:
                AppConfig.cleanApplicationData(getContext(),"");
                AppConfig.showToast(getContext(),getString(R.string.tip_clear_cache));
                break;
            case R.id.rl_version:
                BDAutoUpdateSDK.uiUpdateAction(this.getContext(), new UICheckUpdateCallback() {
                    @Override
                    public void onNoUpdateFound() {
                        AppConfig.showToast(getContext(),getString(R.string.latest_version));
                    }
                    @Override
                    public void onCheckComplete() {

                    }
                });
                break;
            default:
                break;
        }
    }
//    private ListView listView;
//    private ArrayAdapter<String>arrayAdapter;

//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_me, container, false);

//        listView = view.findViewById(R.id.listView1);
//        String[] arr = {"分享APP", "打赏开发者", "建议", "检查版本"};
//        arrayAdapter = new ArrayAdapter<String>(this.getContext(),android.R.layout.simple_list_item_1,arr);
//        listView.setAdapter(arrayAdapter);
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                New(position);
//            }
//        });

//        return view;
//    }
}
