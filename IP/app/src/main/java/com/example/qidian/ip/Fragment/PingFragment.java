package com.example.qidian.ip.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.qidian.ip.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class PingFragment extends Fragment {
    protected View mView;
    protected String mContent;
    EditText mEtText;
    Button mBtPing;
    TextView mTvPing;
    StringBuffer mResult = new StringBuffer();
    PingTask mTask;

    public static PingFragment newInstance() {
        return new PingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_ping, container, false);
            setupViews();
        }
        return mView;
    }

    private void setupViews() {
        mEtText =  mView.findViewById(R.id.et_text);
        mEtText.setText("Baidu");
        mBtPing = mView.findViewById(R.id.bt_create);
        mBtPing.setText("Ping");
        mTvPing = mView.findViewById(R.id.tv_ping);
        mBtPing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()){
                    if (mTask != null){
                        mTask.cancel(true);
                        mTask = null;
                    }else {
                        mTask = new PingTask();
                        mTask.execute(mContent);
                    }
                }
            }
        });
    }

    private boolean isValid() {
        mContent = mEtText.getText().toString();
        if (!TextUtils.isEmpty(mContent)){
//            String regExp =  "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$";
//            Pattern p = Pattern.compile(regExp);
//            Matcher m = p.matcher(mContent);
//            boolean ret = m.matches();
//            if (!ret){
//                AppConfig.showLongToast(this.getContext(),R.string.tip_correct_tel);
//            }
            return true;
        }else{
            return false;
        }
    }

    class  PingTask  extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            mBtPing.setText("Stop");
            mResult.setLength(0);
            mResult.append("ping "+mEtText.getText().toString()+"\n");
            mTvPing.setText(mResult.toString());

        }

        @Override
        protected String doInBackground(String... params) {
            BufferedReader reader = null;
            try {
                Process process = Runtime.getRuntime().exec(
                        "/system/bin/ping -c 8 " + params[0]);
                reader = new BufferedReader(new InputStreamReader(
                        process.getInputStream()));
                int i;
                char[] buffer = new char[1024];
                while (!isCancelled() && ((i = reader.read(buffer))) > 0){
                    String s = new String(buffer,0,i);
                    publishProgress(s);
                    Log.e(" PING ", s);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            mResult.append(values[0]);
            mTvPing.setText(mResult.toString());
        }

        @Override
        protected void onPostExecute(String s) {
            mBtPing.setText("Ping");
            mResult.append("end");
            mTvPing.setText(mResult.toString());
        }

        @Override
        protected void onCancelled() {
            mBtPing.setText("Ping");
            mResult.append("canceled");
            mTvPing.setText(mResult.toString());
        }
    };
}
