package com.example.qidian.ip.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.qidian.ip.R;
import com.example.qidian.ip.ll.AppConfig;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryFragment extends Fragment {

    protected View mView;
    EditText mEtText;
    protected OnQueryListener mListener;
    protected String mContent;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof QueryFragment.OnQueryListener) {
            mListener = (QueryFragment.OnQueryListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_query, container, false);
            setupViews();
        }

        return mView;

    }

    private void setupViews() {
        mEtText = mView.findViewById(R.id.editText);
        mEtText.setText("183.232.231.173");
        mView.findViewById(R.id.bottom1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid()){
                    if (mListener != null) {
                        mListener.onQuery(mContent);
                    }
                }
            }
        });
    }

    private boolean isValid() {
        mContent = mEtText.getText().toString();
        if (!TextUtils.isEmpty(mContent)){
            String regExp =  "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$";
            Pattern p = Pattern.compile(regExp);
            Matcher m = p.matcher(mContent);
            boolean ret = m.matches();
            if (!ret){
                AppConfig.showLongToast(this.getContext(),R.string.ll);
            }
            return ret;
        }else{
            return false;
        }
    }

    public interface OnQueryListener{
        void onQuery(String number);
    }

}
