package com.example.qidian.ip.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.qidian.ip.lll.App;
import com.example.qidian.ip.lll.HistoryDatabase;
import com.example.qidian.ip.ll.BottomNavigationViewHelper;
import com.example.qidian.ip.Fragment.HistoryFragment;
import com.example.qidian.ip.Fragment.MeFragment;
import com.example.qidian.ip.Fragment.PingFragment;
import com.example.qidian.ip.Fragment.QueryFragment;
import com.example.qidian.ip.R;
import com.example.qidian.ip.Adapter.ViewPagerAdapter;
import com.example.qidian.ip.ll.AppConfig;
import com.example.qidian.ip.ll.QueryRequest;
import com.example.qidian.ip.ll.QueryResult;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements HistoryFragment.OnListFragmentInteractionListener, QueryFragment.OnQueryListener{

    ViewPager viewPager;
    MenuItem menuItem;
    BottomNavigationView bottomNavigationView;
    RequestQueue mRequestQueue;
    AlertDialog mDeleteDialog;
    HistoryFragment mHistoryFragment;
    QueryFragment queryFragment;
    PingFragment pingFragment;
    MeFragment meFragment;

    HistoryDatabase mDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout v = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_main,null);
        setContentView(v);
//        setContentView(R.layout.activity_main);
        initData();
        new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);


        viewPager = v.findViewById(R.id.viewpager);
        bottomNavigationView = findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.navigation_query:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.navigation_ping:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.navigation_history:
                                viewPager.setCurrentItem(2);
                                break;
                            case R.id.navigation_mine:
                                viewPager.setCurrentItem(3);
                                break;
                        }
                        return false;
                    }
                });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (menuItem != null) {
                    menuItem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                menuItem = bottomNavigationView.getMenu().getItem(position);
                menuItem.setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

       // 禁止ViewPager滑动
//        viewPager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return true;
//            }
//        });
        setupViewPager(viewPager);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(viewPager.getCurrentItem()==0){
//            startActivityForResult(new Intent(this, SettingsActivity.class), SETTING_CODE);
        } else{
            if (!App.sHistoryList.isEmpty()){
                onClearAllRecords();
            }
        }
        return true;
    }

    private void onClearAllRecords(){
        if (mDeleteDialog ==null) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(R.string.aa);
            alertDialog.setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    for (String number: App.sHistoryList){
                        mDataBase.delete(number);
                    }
                    App.sHistoryList.clear();
                    mHistoryFragment.refreshUI(true);
                }
            });
            alertDialog.setNegativeButton(R.string.cancel, null);
            mDeleteDialog = alertDialog.create();
        }
        mDeleteDialog.show();
    }


    private void setupViewPager(ViewPager viewPager) {
        mHistoryFragment = new HistoryFragment();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new QueryFragment());
        pingFragment = PingFragment.newInstance();
        adapter.addFragment(pingFragment);
        mHistoryFragment = HistoryFragment.newInstance();
        adapter.addFragment(mHistoryFragment);
        meFragment = MeFragment.newInstance();
        adapter.addFragment(meFragment);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onQuery(final String content) {
        QueryRequest stringRequest = new QueryRequest(content,
                new Response.Listener<QueryResult>() {
                    @Override
                    public void onResponse(QueryResult response) {
                        handleResult(response,content);
                    }
                }, new Response.ErrorListener() {
     @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("MainActivity: "," error: "+error);
                AppConfig.showLongToast(MainActivity.this,R.string.ll);
            }
        });
        QueryResult response = stringRequest.getCachedResult(mRequestQueue);
        if (response != null){
            handleResult(response,content);
        }else{
            mRequestQueue.add(stringRequest);
        }
    }

    @Override
    public void onItemClicked(String item) {
        onQuery(item);
    }

    @Override
    public void onItemDeleted(String number) {
        mDataBase.delete(number);
        mHistoryFragment.refreshUI(true);
    }

    private void handleResult(QueryResult response,final String content){
        mDataBase.insert(content);
        if (App.sHistoryList.contains(content)){
            App.sHistoryList.remove(content);
        }
        App.sHistoryList.add(0,content);
        mHistoryFragment.refreshUI(false);

        Intent i = new Intent(MainActivity.this,ResultActivity.class);
        i.putExtra(ResultActivity.RESULT,response);
        startActivity(i);
    }

    private void initData() {
        mRequestQueue = Volley.newRequestQueue(this);
        App.sHistoryList.clear();
        mDataBase = HistoryDatabase.shareInstance(this);
        ArrayList<String> data = mDataBase.readAll();
        Collections.reverse(data);
        App.sHistoryList.addAll(data);

    }

}

