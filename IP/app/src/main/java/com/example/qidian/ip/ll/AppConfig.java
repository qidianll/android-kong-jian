package com.example.qidian.ip.ll;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.widget.Toast;


import com.alibaba.sdk.android.feedback.impl.FeedbackAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by cfans on 2017/6/22.
 */

public class AppConfig {

    /**
     * 软件根目录
     */
    public final static String ROOT_DIR = Environment.getExternalStorageDirectory()
            .getAbsolutePath();
    /**
     * 软件根目录
     */
    public final static String APP_DIR = "/CF_IPQUERY";

    /**
     * 二维码存放目录
     */
    public final static String QR_DIR = "/Code";
    /**
     * 临时文件存放目录
     */
    public final static String TEMP_DIR = "/.Temp";


    /**
     * 阿里用户反馈接口，主要修改ID和SecretID
     */
    public static void  initFeedbackAPI(Activity ctx) {
        FeedbackAPI.init(ctx.getApplication(), "24719956","b815d44d4b561084dff6f4053ca1e6f7");
    }

    public static String getVersionName(Context cx) {
        String packName = cx.getPackageName();
        PackageInfo pinfo = null;
        try {
            pinfo = cx.getPackageManager().getPackageInfo(packName, 0);
        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();
        }
        if (pinfo != null)
            return pinfo.versionName;
        else
            return "1.0.1";
    }

    public static  boolean checkPermissionOK(Activity context, String permission){
        int result = ContextCompat.checkSelfPermission(context,permission);
        if (result != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context, new String[]{permission}, 1);
            return false;
        }
        return true;
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context,text,Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, int text) {
        Toast.makeText(context,text,Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(Context context, String text) {
        Toast.makeText(context,text,Toast.LENGTH_LONG).show();
    }

    public static void showLongToast(Context context, int text) {
        Toast.makeText(context,text,Toast.LENGTH_LONG).show();
    }

    public static void systemShareText(Context activity, String text) {
        systemShareTextWithImage(activity,text,null);
    }

    public static void systemShareImage(Context activity, File file) {
        Uri uri = FileProvider.getUriForFile(activity, "cfans.app.qrgen", file);
        systemShareTextWithImage(activity,null,uri);
//        systemShareTextWithImage(activity,null,Uri.fromFile(file));
    }

    public static void systemShareTextWithImage(Context activity, String text, Uri uri) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        if (uri == null){
            sendIntent.setType("text/plain");
        }else{
            sendIntent.setType("image/*");
            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
        }
        if (text != null){
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        }
        activity.startActivity(sendIntent);
    }

    public static String getAppDir(){
        String appDir = ROOT_DIR+APP_DIR;
        new File(appDir+QR_DIR).mkdirs();
        new File(appDir+TEMP_DIR).mkdirs();
        return appDir;
    }


    public static String getNameWithPrefix(String prefix) {
        StringBuffer s = new StringBuffer(getAppDir());
        s.append(QR_DIR);
        s.append(File.separator);
        s.append(curTimeToQRFileName(prefix));
        return s.toString();
    }

    /**
     * 删除filePath代表的这个文件，或者此目录。
     *
     * @param filePath 文件全名，或者目录
     */
    public static boolean deleteFile(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }
        File file = new File(filePath);
        return deleteFile(file);
    }

    /**
     * 删除file这个文件，或者此目录,需要权限
     *
     * @param file 文件，或者目录
     */
    public static boolean deleteFile(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                deleteFile(f);
            }
        }
        return file.delete();
    }

    public static String curTimeToQRFileName(String suffix) {
        return suffix+longToFormatString(System.currentTimeMillis(),
                "yyyyMMdd_HH:mm:ss")+".png";
    }

    // yyyy-MM-dd HH:mm:ss 代表24小时制
    // yyyy-MM-dd hh:mm:ss 代表12小时制
    private static String longToFormatString(long milliseconds, String timeFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        SimpleDateFormat format = new SimpleDateFormat(timeFormat);
        return format.format(calendar.getTime());
    }

    public static boolean saveBitmap(File file, Bitmap b) {
        if (b != null && file != null) {
            FileOutputStream out;
            try {
                file.createNewFile();
                out = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.PNG, 80, out);
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }


    public static boolean saveBitmap(String path, Bitmap b) {
        if (!TextUtils.isEmpty(path) && b != null) {
            FileOutputStream out = null;
            try {
                File file = new File(path);
                if(file ==null){
                    return false;
                }
                out = new FileOutputStream(file);
                b.compress(Bitmap.CompressFormat.PNG, 80, out);
                out.flush();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }


    private static final String CONFIG = "AppConfig";

    //private method
    private static SharedPreferences getSharedPreferences(Context c) {
        return c.getSharedPreferences(CONFIG,
                Context.MODE_PRIVATE);
    }

    /**清除本应用所有的数据
     *@param context
     * @param filepath
     */
    public static void cleanApplicationData(Context context, String... filepath) {
        /** * 清除本应用内部缓存(/data/data/com.xxx.xxx/cache) */
        deleteFilesByDirectory(context.getCacheDir());
        cleanExternalCache(context);
        /** * 清除本应用所有数据库(/data/data/com.xxx.xxx/databases)*/
        deleteFilesByDirectory(new File("/data/data/" + context.getPackageName() + "/databases"));
        /**清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs) */
        deleteFilesByDirectory(new File("/data/data/"+ context.getPackageName() +"/shared_prefs"));
        deleteFilesByDirectory(context.getFilesDir());    /** * 清除/data/data/com.xxx.xxx/files下的内容*/
        for (String filePath : filepath) {
            deleteFilesByDirectory(new File(filePath));
        }
        getSharedPreferences(context).edit().clear().commit();
    }
    /**
     * * 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache)
     * @param
     * context
     */
    public static void cleanExternalCache(Context context) {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            deleteFilesByDirectory(context.getExternalCacheDir());
        }
    }
    /**
     * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理
     * @param directory
     */
    private static void deleteFilesByDirectory(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                item.delete();
            }
        }
    }

    /**
     * 判断支付宝客户端是否已安装，建议调用转账前检查
     * @param context Context
     * @return 支付宝客户端是否已安装
     */
    private static final String ALIPAY_PACKAGE_NAME = "com.eg.android.AlipayGphone";
    // 旧版支付宝二维码通用 Intent Scheme Url 格式
    private static final String INTENT_URL_FORMAT = "intent://platformapi/startapp?saId=10000007&" +
            "clientVersion=3.7.0.0718&qrcode=https%3A%2F%2Fqr.alipay.com%2F{urlCode}%3F_s" +
            "%3Dweb-other&_t=1472443966571#Intent;" +
            "scheme=alipayqr;package=com.eg.android.AlipayGphone;end";

    public static boolean hasInstalledAlipayClient(Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(ALIPAY_PACKAGE_NAME, 0);
            return info != null;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 打开转账窗口
     * 旧版支付宝二维码方法，需要使用 https://fama.alipay.com/qrcode/index.htm 网站生成的二维码
     * 这个方法最好，但在 2016 年 8 月发现新用户可能无法使用
     *
     * @param activity Parent Activity
     * @param urlCode 手动解析二维码获得地址中的参数，例如 https://qr.alipay.com/aehvyvf4taua18zo6e 最后那段
     * @return 是否成功调用
     */
    public static boolean startAlipayClient(Activity activity, String urlCode) {
        if (AppConfig.hasInstalledAlipayClient(activity)){
            return startIntentUrl(activity, INTENT_URL_FORMAT.replace("{urlCode}", urlCode));
        }else{
            return false;
        }
    }

    /**
     * 打开 Intent Scheme Url
     *
     * @param activity Parent Activity
     * @param intentFullUrl Intent 跳转地址
     * @return 是否成功调用
     */
    public static boolean startIntentUrl(Activity activity, String intentFullUrl) {
        try {
            Intent intent = Intent.parseUri(
                    intentFullUrl,
                    Intent.URI_INTENT_SCHEME
            );
            activity.startActivity(intent);
            return true;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return false;
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}
